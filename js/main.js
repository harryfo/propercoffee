// Creating Firebase Reference
var fireBase = new Firebase("https://proper-coffee.firebaseio.com/");

// Creating Reference to shoplist created by propercoffeeadmin
var shopList = new Firebase("https://proper-coffee.firebaseio.com/shops");

//creating reference to category list
var categories = new Firebase("https://proper-coffee.firebaseio.com/cats");

//Creating Geofire Reference
var geoFire = new GeoFire(fireBase.child("geofire"));

//listener for users login status
fireBase.onAuth(userLoggedIn);

function logOut() {
    fireBase.unauth();
    document.getElementById("signindiv").style.display = "block";
    document.getElementById("loggedin").style.display = "none";
    window.userId = [];
};

//global variables
var shopIDGlobal = [];
var shopImageGlobal = [];
var shopNameGlobal = [];
var currentCatInView = [];
var currentMenuItemId = [];
var arrayOfOrders = [];
var currentOrderInView = 0;

//adding map layer type
var layer = new L.StamenTileLayer("watercolor");
var map = new L.map("map", {zoomControl:false}).locate({setView: true, maxZoom: 16});
map.addLayer(layer);

map.options.minZoom = 12;
map.options.maxZoom = 17;

//defining coffee shop marker image
var coffeeIcon = L.icon({
    iconUrl: './img/shopmarker.png',
    iconSize:     [30, 60], // size of the icon
    iconAnchor:   [18, 50], // point of the icon which will correspond to marker's location
    popupAnchor:  [-3, -50] // point from which the popup should open relative to the iconAnchor
});

function beansClick() {
    window.alert("This version of the application is for demonstrative purposes and does not support viewing beans.");
};

function rewardsClick() {
    window.alert("This version of the application is for demonstrative purposes and does not support viewing rewards");
};

function paymentClick() {
    window.alert("This version of the application is for demonstrative purposes - payment is not required.");
};

//infopage functions
function userLoggedIn(authData) {
//    console.log(authData);
    if (authData) {
        var userId = authData.uid;
        window.userId = userId;
        console.log("Authenticated successfully with payload:", authData);
        document.getElementById("signindiv").style.display = "none";
        document.getElementById("loggedin").style.display = "block";
        fireBase.child("users").child(window.userId).on("value", function(snapshot) {
            var userObject = (snapshot.val());
            firstName = userObject.firstname;
            document.getElementById("profilename").innerHTML = firstName;
        });
        fireBase.child("users").child(window.userId).child("orders").child("activeorders").on("value", function(snapshot) {
        //All active orders stored by user
        var activeOrders = (snapshot.val());
        var totalPrice = [];
        if (activeOrders) {
            document.getElementById('leftorderitems').innerHTML = "";
            document.getElementById("currentorders").style.display = "block";
            document.getElementById("noorders").style.display = "none";
            document.getElementById("instructorder").style.display = "none";
            arrayOfOrders = [];
            $.each(activeOrders, function(orderShopId, orderObj) {
                $.each(orderObj, function(orderId, orderObject) {
                    arrayOfOrders.push({ id: orderShopId, value: orderObject });
                });
                document.getElementById('leftorderitems').innerHTML = "";
                document.getElementById('rightorderitems').innerHTML = "";
                var numberOfOrders = arrayOfOrders.length;
                if (numberOfOrders > 1) {
                    document.getElementById("nextorderbut").style.display = "block";
                    document.getElementById("prevorderbut").style.display = "block";
                    document.getElementById("ordernavig").style.marginTop = "20px";
                } else {
                    document.getElementById("nextorderbut").style.display = "none";
                    document.getElementById("prevorderbut").style.display = "none";
                    document.getElementById("ordernavig").style.marginTop = "15px"; 
                };
                document.getElementById('ordernavig').innerHTML = "Order 1 of " + numberOfOrders;
                });
                if (arrayOfOrders[currentOrderInView] == null) {
                    currentOrderInView = 0;
                };
                var orderShopIdInArray = arrayOfOrders[currentOrderInView].id;
                shopList.child(orderShopIdInArray).child("shopName").once("value", function(snapshot) {
                var orderShopName = (snapshot.val());
                document.getElementById("ordershopname").innerHTML = orderShopName;   
                });
                var orderObject = arrayOfOrders[currentOrderInView].value;
                var orderStatus = orderObject.status;
                if (orderStatus == "Placed") {
                    document.getElementById("statuscup").src = "img/statuscup2.gif";
                } else if (orderStatus == "Accepted") {
                    document.getElementById("statuscup").src = "img/statuscup3.gif";
                } else if (orderStatus == "Ready") {
                    document.getElementById("statuscup").src = "img/statuscup4.gif";
                } else {
                    document.getElementById("statuscup").src = "img/statuscup2.gif";
                };
                document.getElementById("rightorderinfo").innerHTML = "Order " + orderStatus +"!";
                var orderShopIdInArray = arrayOfOrders[currentOrderInView].id;
                var orderObject = arrayOfOrders[currentOrderInView].value;
                var orderContent = orderObject.order;
                document.getElementById('rightorderitems').innerHTML = "";
                $.each(orderContent, function(orderItemId, orderItem) {
                    var itemName = orderItem.itemName;
                    var itemQuantity = orderItem.quantity;
                    var itemToAdd = document.createElement('div');
                    itemToAdd.innerHTML = itemName + " x " + itemQuantity;
                    document.getElementById('leftorderitems').appendChild(itemToAdd);
                    var itemCat = orderItem.itemCat;
                    var priceToAdd = document.createElement('div');
                    shopList.child(orderShopIdInArray).child("menuItems").child(itemCat).child(orderItemId).child("price").once("value", function(snapshot) {
                        var itemPrice = (snapshot.val());
                        var priceInt = parseFloat(itemPrice);
                        var multiplyInt = priceInt *= itemQuantity;
                        totalPrice.push(multiplyInt);
                        priceToAdd.innerHTML = "£" + itemPrice;
                        var total = 0;
                        $.each(totalPrice, function(){
                            total += this;
                        });
                        document.getElementById('rightordertotal').innerHTML = "£" + total.toFixed(2);
                    });
                    document.getElementById('rightorderitems').appendChild(priceToAdd);
                })
        } else {
            document.getElementById('leftorderitems').innerHTML = "";
            document.getElementById('rightorderitems').innerHTML = "";
            document.getElementById("currentorders").style.display = "none";
            document.getElementById("noorders").style.display = "block";
            document.getElementById("instructorder").style.display = "block";
        };
     });
    } else {
        console.log("User logged out");
        window.userId = [];
    }
};

function nextOrderClick() {
    var totalPrice = [];
    document.getElementById('leftorderitems').innerHTML = "";
    document.getElementById('rightorderitems').innerHTML = "";
    document.getElementById('rightordertotal').innerHTML = "";
    var numberOfOrders = arrayOfOrders.length;
    currentOrderInView += 1;
    if (currentOrderInView >= (numberOfOrders)) {
        currentOrderInView = 0;
    } else {
    }
        var x = currentOrderInView;
        document.getElementById('ordernavig').innerHTML = "Order " + (x += 1) + " of " + numberOfOrders;
        var orderShopIdInArray =  arrayOfOrders[currentOrderInView].id;
        shopList.child(orderShopIdInArray).child("shopName").once("value", function(snapshot) {
            var orderShopName = (snapshot.val());
            document.getElementById("ordershopname").innerHTML = orderShopName;   
        });
        var orderObject = arrayOfOrders[currentOrderInView].value;
        var orderStatus = orderObject.status;
        if (orderStatus == "Placed") {
            document.getElementById("statuscup").src = "img/statuscup2.gif";
        } else if (orderStatus == "Accepted") {
            document.getElementById("statuscup").src = "img/statuscup3.gif";
        } else if (orderStatus == "Ready") {
            document.getElementById("statuscup").src = "img/statuscup4.gif";
        } else {
            document.getElementById("statuscup").src = "img/statuscup2.gif";
        };
        document.getElementById("rightorderinfo").innerHTML = "Order " + orderStatus +"!";
        var orderContent = orderObject.order;
        $.each(orderContent, function(orderItemId, orderItem) {
            var itemName = orderItem.itemName;
            var itemQuantity = orderItem.quantity;
            var itemToAdd = document.createElement('div');
            itemToAdd.innerHTML = itemName + " x " + itemQuantity;
            document.getElementById('leftorderitems').appendChild(itemToAdd);
            var itemCat = orderItem.itemCat;
            var priceToAdd = document.createElement('div');
            shopList.child(orderShopIdInArray).child("menuItems").child(itemCat).child(orderItemId).child("price").once("value", function(snapshot) {
                var itemPrice = (snapshot.val());
                var priceInt = parseFloat(itemPrice);
                var multiplyInt = priceInt *= itemQuantity;
                totalPrice.push(multiplyInt);
                priceToAdd.innerHTML = "£" + itemPrice;
                var total = 0;
                $.each(totalPrice, function(){
                    total += this;
                });
                document.getElementById('rightordertotal').innerHTML = "£" + total.toFixed(2);
                document.getElementById("loadinghold").style.display = "none";
            });
            document.getElementById('rightorderitems').appendChild(priceToAdd);

        })
};

function prevOrderClick() {
    var totalPrice = [];
    document.getElementById('leftorderitems').innerHTML = "";
    document.getElementById('rightorderitems').innerHTML = "";
    document.getElementById('rightordertotal').innerHTML = "";
    var numberOfOrders = arrayOfOrders.length;
    console.log(numberOfOrders);
    currentOrderInView -= 1;
    var y = arrayOfOrders.length
    console.log(currentOrderInView);
    if (currentOrderInView <= -1) {
        currentOrderInView = (y -=1);
    } else {
    }
    var x = currentOrderInView;
    document.getElementById('ordernavig').innerHTML = "Order " + (x += 1) + " of " + numberOfOrders;
    var orderShopIdInArray =  arrayOfOrders[currentOrderInView].id;
    console.log(orderShopIdInArray);
    shopList.child(orderShopIdInArray).child("shopName").once("value", function(snapshot) {
        var orderShopName = (snapshot.val());
        document.getElementById("ordershopname").innerHTML = orderShopName;   
        });
    var orderObject = arrayOfOrders[currentOrderInView].value;
    var orderStatus = orderObject.status;
    if (orderStatus == "Placed") {
        document.getElementById("statuscup").src = "img/statuscup2.gif";
    } else if (orderStatus == "Accepted") {
        document.getElementById("statuscup").src = "img/statuscup3.gif";
    } else if (orderStatus == "Ready") {
        document.getElementById("statuscup").src = "img/statuscup4.gif";
    } else {
        document.getElementById("statuscup").src = "img/statuscup2.gif";
    };
    document.getElementById("rightorderinfo").innerHTML = "Order " + orderStatus +"!";
    var orderContent = orderObject.order;
    $.each(orderContent, function(orderItemId, orderItem) {
        var itemName = orderItem.itemName;
        var itemQuantity = orderItem.quantity;
        var itemToAdd = document.createElement('div');
        itemToAdd.innerHTML = itemName + " x " + itemQuantity;
        document.getElementById('leftorderitems').appendChild(itemToAdd);
        var itemCat = orderItem.itemCat;
        var priceToAdd = document.createElement('div');
        shopList.child(orderShopIdInArray).child("menuItems").child(itemCat).child(orderItemId).child("price").once("value", function(snapshot) {
            var itemPrice = (snapshot.val());
            var priceInt = parseFloat(itemPrice);
            var multiplyInt = priceInt *= itemQuantity;
            totalPrice.push(multiplyInt);
            priceToAdd.innerHTML = "£" + itemPrice;
            var total = 0;
            $.each(totalPrice, function(){
                total += this;
            });
            document.getElementById('rightordertotal').innerHTML = "£" + total.toFixed(2);
        });
        document.getElementById('rightorderitems').appendChild(priceToAdd);
    })
};

function backToMapNotSigned() {
    document.getElementById("menupage").style.display = "none";
    document.getElementById("map").style.display = "block";
};

//register button pressed
function register() {
    document.getElementById("infopage").style.display = "block";
    document.getElementById("menupage").style.display = "none";
    document.getElementById("popupbackground").style.display = "none";
    document.getElementById("popuplightbox").style.display = "none";
    document.getElementById("signindiv").style.display = "none";
    $('#registerdiv').css("-webkit-animation", "fadein 0.5s");
    $('#registerdiv').css("-webkit-animation-iteration-count", "1");
    $('#registerdiv').css("animation", "fadein 0.5s");
    $('#registerdiv').css("animation-iteration-count", "1");
    $('#registerdiv').css("-webkit-animation-fill-mode", "forwards");
    $('#registerdiv').css("animation-fill-mode", "forwards");
    document.getElementById("registerdiv").style.display = "block";
    document.getElementById("infopage").style.display = "block";
};

//register button pressed
function registerMenu() {
    //info page slide in animation
    $('#infopage').css("-webkit-animation", "slidein 0.5s");
    $('#infopage').css("-webkit-animation-iteration-count", "1");
    $('#infopage').css("animation", "slidein 0.5s");
    $('#infopage').css("animation-iteration-count", "1");
    $('#infopage').css("-webkit-animation-fill-mode", "forwards");
    $('#infopage').css("animation-fill-mode", "forwards");
    document.getElementById("infopage").style.display = "block";
    document.getElementById("menupage").style.display = "none";
    document.getElementById("popupbackground").style.display = "none";
    document.getElementById("popuplightbox").style.display = "none";
    document.getElementById("signindiv").style.display = "none";
    $('#registerdiv').css("-webkit-animation", "fadein 0.5s");
    $('#registerdiv').css("-webkit-animation-iteration-count", "1");
    $('#registerdiv').css("animation", "fadein 0.5s");
    $('#registerdiv').css("animation-iteration-count", "1");
    $('#registerdiv').css("-webkit-animation-fill-mode", "forwards");
    $('#registerdiv').css("animation-fill-mode", "forwards");
    document.getElementById("registerdiv").style.display = "block";
    document.getElementById("infopage").style.display = "block";
    categories.on("value", function(snapshot) {
        var categoriesList = (snapshot.val());
        $.each(categoriesList, function(categoryId) {
            var destroyCat = document.getElementById(categoryId);
            if (destroyCat) {
            destroyCat.remove();
            }
        }) 
    })
    document.getElementById("itemscont").innerHTML = "";
};

//second register form page displayed
function nextPageRegister(form) {
    password = form.password.value;
    verpassword = form.verpassword.value;
    email = form.email.value;
    veremail = form.veremail.value;
    if (email == veremail) {
        if (password == verpassword) {
            document.getElementById("formstatus").style.display = "none";
            document.getElementById("registerpageone").style.display = "none";
            $('#registerpagetwo').css("-webkit-animation", "fadein 0.5s");
            $('#registerpagetwo').css("-webkit-animation-iteration-count", "1");
            $('#registerpagetwo').css("animation", "fadein 0.5s");
            $('#registerpagetwo').css("animation-iteration-count", "1");
            $('#registerpagetwo').css("-webkit-animation-fill-mode", "forwards");
            $('#registerpagetwo').css("animation-fill-mode", "forwards");
            document.getElementById("registerpagetwo").style.display = "block";
        } else {
            window.alert("Passwords don't match. Please check and try again.");
            document.getElementById("formstatus").style.display = "block";
        }
    } else {
        window.alert("Emails don't match. Please check and try again.");
        document.getElementById("formstatus").style.display = "block";
    }
};

//created consumer user in firebase
function createUser(form) {
    password = form.password.value;
    email = form.email.value;
    firstname = form.firstname.value;
    lastname = form.lastname.value;
    age = form.age.value;
    city = form.city.value;
    fireBase.createUser({
        email : email,
        password : password
    }, function(error, userData) {
        //if creating a new user fails
        if (error) {
            console.log("Error creating user:", error);
            document.getElementById("formstatus").innerHTML = "Error creating user";
        } else {
            //creating a new user was a success
            console.log("Successfully created user account with uid:", userData.uid);
            //creates variable for unique user id
            var userId = userData.uid;
            //logs user in
            fireBase.authWithPassword({
                email : email,
                password : password
            }, function(error, authData) {
                if (error) {
                    console.log("Login Failed!", error);
                    document.getElementById("formstatus").innerHTML = "Error logging into new account";
                } else {
                    fireBase.child("users").child(authData.uid).set({
                        firstname: firstname,
                        lastname: lastname,
                        city: city,
                        age: age
                    })
                    document.getElementById("registerdiv").style.display = "none";
                    $('#loggedin').css("-webkit-animation", "fadein 0.5s");
                    $('#loggedin').css("-webkit-animation-iteration-count", "1");
                    $('#loggedin').css("animation", "fadein 0.5s");
                    $('#loggedin').css("animation-iteration-count", "1");
                    $('#loggedin').css("-webkit-animation-fill-mode", "forwards");
                    $('#loggedin').css("animation-fill-mode", "forwards");
                    document.getElementById("loggedin").style.display = "block";
                }
            })
        }
    })
};

function loginFormSubmitted(form) {
    var email = form.email.value;
    var password = form.password.value;
    //log in using firebase authentication
    fireBase.authWithPassword({
        email : email,
        password : password
    }, function(error, authData) {
        //if log in is unsuccessful
        if (error) {
            console.log("Login Failed!", error);
            window.alert("Login Failed: Username or Password Incorrect");
        //if log in is successful
        } else {
            console.log("Authenticated successfully with payload:", authData);
            //create variable for the users unique user ID
            var userID = authData.uid;
            //get the data from the entry in the shop list matching the user ID
            document.getElementById("signin").reset();
            document.getElementById("signindiv").style.display = "none";
            document.getElementById("lightboxmenu").style.display = "none";
            document.getElementById("signinformenu").style.display = "none";
            document.getElementById("loggedin").style.display = "block";
            document.getElementById("registerdiv").style.display = "none";
        }
    })
};

//When location is found run location found function
map.on('locationfound', onLocationFound);

//when the browser has detected the users location
function onLocationFound(e) {
    //Change radius of circle to represent accuracy of location detection
    var radius = e.accuracy / 2;
    //variables which show the users latitude and longitude
    var userLat = e.latlng.lat;
    var userLng = e.latlng.lng;
    var center = [userLat, userLng];
    
    //setting the lat and lng of the center of the current map view
    var latOfMapView = map.getCenter().lat
    var lngOfMapView = map.getCenter().lng
    centerOfMapView = [latOfMapView, lngOfMapView];
    
    //updates query when user moves the map
    map.on("move", function (e) {
    var latOfMapView = map.getCenter().lat
    var lngOfMapView = map.getCenter().lng
    centerOfMapView = [latOfMapView, lngOfMapView];
    geoQuery.updateCriteria({
        center: centerOfMapView
        });
    });
    
    //circle which shows accuracy of the users location    
    var circleAccuracyOptions = {
    radius: 16,
    color: '#FFFFFF',
    fillColor: '#bd9ac7',
    fillOpacity: 0.6,
    weight: 4
    };
    
    var circleMarkerOptions = {
    radius: 8,
    color: '#FFFFFF',
    fillColor: '#bd9ac7',
    fillOpacity: 1,
    opacity: 1,
    weight: 2
    };
    
    var locationAccuracy = L.circleMarker(e.latlng, circleAccuracyOptions)
    .addTo(map);
    
    var locationMarker = L.circleMarker(e.latlng, circleMarkerOptions)
    .addTo(map);
    
    //Creating a variable for shops in query to be stored in
    var shopsInQuery = {};
    
    //Radius for GeoFire to load data from in Kilometers
    var radius = 30;
    //runs the query from users location
    var geoQuery = geoFire.query({
        center: centerOfMapView,
        radius: radius
    });
    
    //only runs if GeoFire shop is within the query
    var onShopDetected = geoQuery.on("key_entered", function(key, location) {
        //uses the GeoFire Key to look up the shops entry in Firebase
        shopList.child(key).once("value", function(snapshot) {
            var shopInQueryData = (snapshot.val());
            //gets name of the shop
            var shopName = shopInQueryData.shopName;
            //gets longitude of shop
            var shopLon = shopInQueryData.lon;
            //gets latitude of shop
            var shopLat = shopInQueryData.lat;
            //gets link of shop
            var shopLink = shopInQueryData.link;
            //gets address of shop
            var shopAddress = shopInQueryData.addressline;
            //gets Opening times of shop
            var shopTime = shopInQueryData.openingtime;
            //gets phone number of shop
            var shopPhone = shopInQueryData.phone;
            //gets post code of shop
            var shopPost = shopInQueryData.postcode;
            //gets wifi status of shop
            var shopWifi = shopInQueryData.wifi;
            //gets shop image
            var shopImage = shopInQueryData.shopImage;
            //gets shop menu reference
            var shopMenu = shopInQueryData.menu;
            //adds marker for shop in range
            var marker = L.marker(location, {icon: coffeeIcon}).on('click', onMarkerClick);
            //adds on click function for when marker is clicked
            function onMarkerClick(e) {
                shopIDGlobal = key;
                shopNameGlobal = shopName;
                shopImageGlobal = shopImage;
                //changes pop up to display relevant info and makes pop up appear
                document.getElementById("coffeeshopname").innerHTML = "<a id='shopnamefont' href='http://" + shopLink + "'>" + shopName + "</a>";
                //changes menu page elements incase pressed
                document.getElementById("shoptitletext").innerHTML = shopName;
                if (shopImage == null) {
                    document.getElementById("gotomenubuttoncont").innerHTML = '<span class="subtext" id="gotomenu">No Menu Available</span><br>';
                } else {
                    document.getElementById("shopheader").src = shopImage;
                    document.getElementById("gotomenubuttoncont").innerHTML = '<a href="javascript:void(0)" onclick="openMenuPage();"><span class="subtext" id="gotomenu">Go to Menu</span><br></a>';
                }
                $('#popupbackground').css("-webkit-animation", "animationpopupback 0.5s");
                $('#popupbackground').css("-webkit-animation-iteration-count", "1");
                $('#popupbackground').css("animation", "animationpopupback 0.5s");
                $('#popupbackground').css("animation-iteration-count", "1");
                $('#popupbackground').css("-webkit-animation-fill-mode", "forwards");
                $('#popupbackground').css("animation-fill-mode", "forwards");
                $('#textinpopup').css("-webkit-animation", "animationpopuptext 0.5s");
                $('#textinpopup').css("-webkit-animation-iteration-count", "1");
                $('#textinpopup').css("animation", "animationpopuptext 0.5s");
                $('#textinpopup').css("animation-iteration-count", "1");
                $('#textinpopup').css("-webkit-animation-fill-mode", "forwards");
                $('#textinpopup').css("animation-fill-mode", "forwards");
                $.each(shopMenu, function(item, value){
                    //snap shot the same category from the cats databse
                    categories.child(item).once("value", function(snapshot) {
                        var categoriesList = (snapshot.val());
                        //get the plain text category name
                        var categoryName = categoriesList.name;
                        var categoryDiv = document.createElement('div');
                        categoryDiv.className = 'categorycontainer';
                        categoryDiv.setAttribute("id", item);
                        categoryDiv.setAttribute("onclick", item + "catClicked();getMenuList();");
                        categoryDiv.innerHTML = '<a href="javascript:void(0)"><img class="menucategory" src="img/' + item + '.png"><div class="menucatoverlay"></div><div class="menucattext">' + categoryName + '</div></a>';
                        document.getElementById('categoriescont').appendChild(categoryDiv);
                        if (value == 1) {
                            document.getElementById(item).style.display = "block";
                        } else {
                            document.getElementById(item).style.display = "none";
                        }
                    })
                })
                //finds day of the week from jQuery
                var  dotw = new Date().getDay();
                // changes opening time to correct times for day
                if (dotw < 1) {
                    document.getElementById("openingtime").innerHTML = "Sun - " + shopTime.openingTimes.sun + " - " + shopTime.closingTimes.sun;
                } else if (dotw < 2) {
                    document.getElementById("openingtime").innerHTML = "Mon - " + shopTime.openingTimes.mon + " - " + shopTime.closingTimes.mon;
                } else if (dotw < 3) {
                    document.getElementById("openingtime").innerHTML = "Tue - " + shopTime.openingTimes.tue + " - " + shopTime.closingTimes.tue;
                } else if (dotw < 4) {
                    document.getElementById("openingtime").innerHTML = "Wed - " + shopTime.openingTimes.wed + " - " + shopTime.closingTimes.wed;
                } else if (dotw < 5) {
                    document.getElementById("openingtime").innerHTML = "Thu - " + shopTime.openingTimes.thu + " - " + shopTime.closingTimes.thu;
                } else if (dotw < 6) {
                    document.getElementById("openingtime").innerHTML = "Fri - " + shopTime.openingTimes.fri + " - " + shopTime.closingTimes.fri;
                } else {
                    document.getElementById("openingtime").innerHTML = "Sat - " + shopTime.openingTimes.sat + " - " + shopTime.closingTimes.sat;
                };
                //changing HTML elements in the popup to show info for shop
                document.getElementById("wifistatus").innerHTML = "Wifi: " + shopWifi;
                document.getElementById("phonenumber").innerHTML = '<a href="tel:' + shopPhone + '">' + shopPhone + '</a>' ;
                document.getElementById("addressline").innerHTML = shopAddress;
                //links to routing function
                document.getElementById("getdirections").innerHTML = '<a href="javascript:void(0)" onclick="getDirectionsLightBoxClose();routeToShop(' + shopLat + ','+ shopLon + ',' + userLat + ',' + userLng + ');">Get Directions</a>';
                document.getElementById("popupbackground").style.display = "block";
                document.getElementById("popuplightbox").style.display = "block";
                document.getElementById("textinpopup").style.display = "block";
                //moves shop to view in the center and zooms
                var shopLatNumber = parseFloat(shopLat);
                var adjustedLat = shopLatNumber - 0.0007;
                map.setView(([adjustedLat, shopLon]), 17, {animate: true} );
    //            //checks for current orders by user
    //            fireBase.child("users").child(window.userId).child("orders").child("activeorders").child(key).once("value", function(snapshot) {
    //            var activeShopOrders = (snapshot.val());
    //            if (activeShopOrders) {
    //                document.getElementById("previousorderscont").style.display = "block";
    //                document.getElementById("previousorderstext").style.display = "block";
    //            } else {
    //                console.log("no previous orders");
    //            };
    //            });
            };
                //puts marker on map
                marker.addTo(map);
                closeSplash();
        });
        //loading images of categories
        var hotdrinksimg = new Image();
        hotdrinksimg.src = "img/hotdrinks.png";
        var colddrinksimg = new Image();
        colddrinksimg.src = "img/colddrinks.png";
        var bakeryimg = new Image();
        bakeryimg.src = "img/bakery.png";
        var fruitimg = new Image();
        fruitimg.src = "img/fruit.png";
        var lunchimg = new Image();
        lunchimg.src = "img/lunch.png";
        var snacksimg = new Image();
        snacksimg.src = "img/snacks.png";
        var statuscup = new Image();
        statuscup.src = "img/statuscup2.gif";
        var statuscupAc = new Image();
        statuscupAc.src = "img/statuscup3.gif";
        var statuscupRe = new Image();
        statuscupRe.src = "img/statuscup4.gif";
    });
};

function closeSplash() {
    $('#splash').css("-webkit-animation", "fadeout 0.5s");
    $('#splash').css("-webkit-animation-iteration-count", "1");
    $('#splash').css("animation", "fadeout 0.5s");
    $('#splash').css("animation-iteration-count", "1");
    $('#splash').css("-webkit-animation-fill-mode", "forwards");
    $('#splash').css("animation-fill-mode", "forwards");
    displayNoneSplash();
};

function displayNoneSplash() {
    document.getElementById("splash").style.display = "none";
}

//Routing using leaflet routing machine
//pulls data in from onLocationFound, users location and shop location
function routeToShop(lat1, lng1, lat2, lng2) {
    //Sets view back to current location
    map.setView(([lat2, lng2]), 16, {animate: true} );
    //creates the waypoints to join the line between
    var routingControl = L.Routing.control({
        waypoints : [
            L.latLng(lat2, lng2),
            L.latLng(lat1, lng1)
        ],
        //hide ititnary contents (not box)
        show: false,
        draggableWaypoints: false,
        addWaypoints: false,
        //hides A and B point markers
        createMarker: function() { return null; },
        lineOptions: {
            styles: [
            {color: 'black', opacity: 0, weight: 0},
            {color: '#755b7d', opacity: 1, weight: 8},
            {color: '#bd9ac7', opacity: 1, weight: 5}
            ]
           },
        //adjust view to fit line in
        show: false
    })
    .addTo(map);
};

var mapCont = document.getElementById("map");

//Reset zoom and show map
function closeLightBox() {
    categories.on("value", function(snapshot) {
        var categoriesList = (snapshot.val());
        $.each(categoriesList, function(categoryId) {
            var destroyCat = document.getElementById(categoryId);
            if (destroyCat) {
            destroyCat.remove();
            }
        }) 
    })
    map.setZoom((16), {animate: true});
    document.getElementById("popuplightbox").style.display = "none";
    document.getElementById("menupage").style.display = "none";
    document.getElementById("menuitem").style.display = "none";
    $('#popupbackground').css("-webkit-animation", "animationpopupbackclose 0.5s");
    $('#popupbackground').css("-webkit-animation-iteration-count", "1");
    $('#popupbackground').css("animation", "animationpopupbackclose 0.5s");
    $('#popupbackground').css("animation-iteration-count", "1");
    $('#popupbackground').css("-webkit-animation-fill-mode", "forwards");
    $('#popupbackground').css("animation-fill-mode", "forwards");
    $('#textinpopup').css("-webkit-animation", "animationpopuptextclose 0.5s");
    $('#textinpopup').css("-webkit-animation-iteration-count", "1");
    $('#textinpopup').css("animation", "animationpopuptextclose 0.5s");
    $('#textinpopup').css("animation-iteration-count", "1");
    $('#textinpopup').css("-webkit-animation-fill-mode", "forwards");
    $('#textinpopup').css("animation-fill-mode", "forwards");
};

//Showing map when lightbox is closed to show directions for fitting the rouite in view
function getDirectionsLightBoxClose() {
    categories.on("value", function(snapshot) {
        var categoriesList = (snapshot.val());
        $.each(categoriesList, function(categoryId) {
            var destroyCat = document.getElementById(categoryId);
            if (destroyCat) {
            destroyCat.remove();
            }
        }) 
    })
    $('#popupbackground').css("-webkit-animation", "animationpopupbackclose 0.5s");
    $('#popupbackground').css("-webkit-animation-iteration-count", "1");
    $('#popupbackground').css("animation", "animationpopupbackclose 0.5s");
    $('#popupbackground').css("animation-iteration-count", "1");
    $('#popupbackground').css("-webkit-animation-fill-mode", "forwards");
    $('#popupbackground').css("animation-fill-mode", "forwards");
    $('#textinpopup').css("-webkit-animation", "animationpopuptextclose 0.5s");
    $('#textinpopup').css("-webkit-animation-iteration-count", "1");
    $('#textinpopup').css("animation", "animationpopuptextclose 0.5s");
    $('#textinpopup').css("animation-iteration-count", "1");
    $('#textinpopup').css("-webkit-animation-fill-mode", "forwards");
    $('#textinpopup').css("animation-fill-mode", "forwards");
    document.getElementById("map").style.display = "block";
};

//switch page to orders page
function openOrdersPage() {
    document.getElementById("lightboxmenu").style.display = "none";
    document.getElementById("signinformenu").style.display = "none";
    //order page slide in animation
    $('#orderpage').css("-webkit-animation", "slidein 0.5s");
    $('#orderpage').css("-webkit-animation-iteration-count", "1");
    $('#orderpage').css("animation", "slidein 0.5s");
    $('#orderpage').css("animation-iteration-count", "1");
    $('#orderpage').css("-webkit-animation-fill-mode", "forwards");
    $('#orderpage').css("animation-fill-mode", "forwards");
    document.getElementById("orderpage").style.display = "block";
    categories.on("value", function(snapshot) {
        var categoriesList = (snapshot.val());
        $.each(categoriesList, function(categoryId) {
            var destroyCat = document.getElementById(categoryId);
            if (destroyCat) {
            destroyCat.remove();
            }
        }) 
    })
    document.getElementById("itemscont").innerHTML = "";
};

//switch page to info page
function openInfoPage() {
    document.getElementById("lightboxmenu").style.display = "none";
    document.getElementById("signinformenu").style.display = "none";
    //info page slide in animation
    $('#infopage').css("-webkit-animation", "slidein 0.5s");
    $('#infopage').css("-webkit-animation-iteration-count", "1");
    $('#infopage').css("animation", "slidein 0.5s");
    $('#infopage').css("animation-iteration-count", "1");
    $('#infopage').css("-webkit-animation-fill-mode", "forwards");
    $('#infopage').css("animation-fill-mode", "forwards");
    document.getElementById("infopage").style.display = "block";
    categories.on("value", function(snapshot) {
        var categoriesList = (snapshot.val());
        $.each(categoriesList, function(categoryId) {
            var destroyCat = document.getElementById(categoryId);
            if (destroyCat) {
            destroyCat.remove();
            }
        }) 
    })
    document.getElementById("itemscont").innerHTML = "";
};

//switch page to menu page
function openMenuPage() {
    //infopage slide out animation
    document.getElementById("popuplightbox").style.display = "none";
    $('#map').css("-webkit-animation", "fadeout 0.3s");
    $('#map').css("-webkit-animation-iteration-count", "1");
    $('#map').css("animation", "fadeout 0.3s");
    $('#map').css("animation-iteration-count", "1");
    $('#map').css("-webkit-animation-fill-mode", "forwards");
    $('#map').css("animation-fill-mode", "forwards");
    $('#popupbackground').css("-webkit-animation", "fadeout 0.3s");
    $('#popupbackground').css("-webkit-animation-iteration-count", "1");
    $('#popupbackground').css("animation", "fadeout 0.3s");
    $('#popupbackground').css("animation-iteration-count", "1");
    $('#popupbackground').css("-webkit-animation-fill-mode", "forwards");
    $('#popupbackground').css("animation-fill-mode", "forwards");
    $('#textinpopup').css("-webkit-animation", "fadeout 0.3s");
    $('#textinpopup').css("-webkit-animation-iteration-count", "1");
    $('#textinpopup').css("animation", "fadeout 0.3s");
    $('#textinpopup').css("animation-iteration-count", "1");
    $('#textinpopup').css("-webkit-animation-fill-mode", "forwards");
    $('#textinpopup').css("animation-fill-mode", "forwards");
    $('#menupage').css("-webkit-animation", "fadein 0.5s");
    $('#menupage').css("-webkit-animation-iteration-count", "1");
    $('#menupage').css("animation", "fadein 0.5s");
    $('#menupage').css("animation-iteration-count", "1");
    $('#menupage').css("-webkit-animation-fill-mode", "forwards");
    $('#menupage').css("animation-fill-mode", "forwards");
    document.getElementById("menupage").style.display = "block";
    document.getElementById("categoriescont").style.display = "block";
    window.setTimeout(hideMap, 500);
};

function hideMap() {
    document.getElementById("map").style.display = "none";
    document.getElementById("popupbackground").style.display = "none";
    document.getElementById("textinpopup").style.display = "none";
};

//removing elements from menu page 1 to make way for menu page 2
function openMenuPage2() {
    var x = document.getElementsByClassName("menucategory");
    var i;
    for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
    }
    var y = document.getElementsByClassName("menucatoverlay");
    var j;
    for (j = 0; j < y.length; j++) {
    y[j].style.display = "none";
    }
    var z = document.getElementsByClassName("menucattext");
    var z;
    for (k = 0; k < z.length; k++) {
    z[k].style.display = "none";
    }
};

//switch page to map page
function openMapPage() {
    document.getElementById("lightboxmenu").style.display = "none";
    document.getElementById("signinformenu").style.display = "none";
    //map slide in animation
    $('#map').css("-webkit-animation", "slidein 0.5s");
    $('#map').css("-webkit-animation-iteration-count", "1");
    $('#map').css("animation", "slidein 0.5s");
    $('#map').css("animation-iteration-count", "1");
    $('#map').css("-webkit-animation-fill-mode", "forwards");
    $('#map').css("animation-fill-mode", "forwards");
    document.getElementById("map").style.display = "block";
    categories.on("value", function(snapshot) {
        var categoriesList = (snapshot.val());
        $.each(categoriesList, function(categoryId) {
            var destroyCat = document.getElementById(categoryId);
            if (destroyCat) {
            destroyCat.remove();
            }
        }) 
    })
    document.getElementById("itemscont").innerHTML = "";
};

//close all pages down
function closeAll() {
    //map slide out animation
    $('#map').css("-webkit-animation", "slideleft 0.5s");
    $('#map').css("-webkit-animation-iteration-count", "1");
    $('#map').css("animation", "slideleft 0.5s");
    $('#map').css("animation-iteration-count", "1");
    $('#map').css("-webkit-animation-fill-mode", "forwards");
    $('#map').css("animation-fill-mode", "forwards");
    //orderpage slide out animation
    $('#orderpage').css("-webkit-animation", "slideleft 0.5s");
    $('#orderpage').css("-webkit-animation-iteration-count", "1");
    $('#orderpage').css("animation", "slideleft 0.5s");
    $('#orderpage').css("animation-iteration-count", "1");
    $('#orderpage').css("-webkit-animation-fill-mode", "forwards");
    $('#orderpage').css("animation-fill-mode", "forwards");
    //infopage slide out animation
    $('#infopage').css("-webkit-animation", "slideleft 0.5s");
    $('#infopage').css("-webkit-animation-iteration-count", "1");
    $('#infopage').css("animation", "slideleft 0.5s");
    $('#infopage').css("animation-iteration-count", "1");
    $('#infopage').css("-webkit-animation-fill-mode", "forwards");
    $('#infopage').css("animation-fill-mode", "forwards");
    document.getElementById("menupage").style.display = "none";
    document.getElementById("menuitem").style.display = "none";
    document.getElementById("popupbackground").style.display = "none";
    document.getElementById("popuplightbox").style.display = "none";
    document.getElementById("textinpopup").style.display = "none";
};

//back function when the user is inside a category and wants to get back to list
function backToCats() {
    document.getElementById("shopheader").style.display = "none";
    var categoriesCont = $('#categoriescont');
    categoriesCont.css("-webkit-animation", "fadein 0.5s");
    categoriesCont.css("-webkit-animation-iteration-count", "1");
    categoriesCont.css("animation", "fadein 0.5s");
    categoriesCont.css("animation-iteration-count", "1");
    categoriesCont.css("-webkit-animation-fill-mode", "forwards");
    categoriesCont.css("animation-fill-mode", "forwards");
    $('#shopheader').css("-webkit-animation", "fadein 0.5s");
    $('#shopheader').css("-webkit-animation-iteration-count", "1");
    $('#shopheader').css("animation", "fadein 0.5s");
    $('#shopheader').css("animation-iteration-count", "1");
    $('#shopheader').css("-webkit-animation-fill-mode", "forwards");
    $('#shopheader').css("animation-fill-mode", "forwards");
    $('#shoptitletext').css("-webkit-animation", "fadein 0.5s");
    $('#shoptitletext').css("-webkit-animation-iteration-count", "1");
    $('#shoptitletext').css("animation", "fadein 0.5s");
    $('#shoptitletext').css("animation-iteration-count", "1");
    $('#shoptitletext').css("-webkit-animation-fill-mode", "forwards");
    $('#shoptitletext').css("animation-fill-mode", "forwards");
    document.getElementById("shoptitletext").innerHTML = shopNameGlobal;
    document.getElementById("categoriescont").style.display = "block";
    document.getElementById("menuitem").style.display = "none";
    document.getElementById("itemscont").innerHTML = "";
    document.getElementById("shopheader").src = shopImageGlobal;
    document.getElementById("shopheader").style.display = "block";
};

function registerBack() {
    document.getElementById("registerdiv").style.display = "none";
    document.getElementById("signindiv").style.display = "block";
};

//completely closes the menu element and sets the inner html empty
function closeMenu() {
    categories.on("value", function(snapshot) {
        var categoriesList = (snapshot.val());
        $.each(categoriesList, function(categoryId) {
            var destroyCat = document.getElementById(categoryId);
            if (destroyCat) {
            destroyCat.remove();
            }
        }) 
    })
    $('#map').css("-webkit-animation", "fadein 0.5s");
    $('#map').css("-webkit-animation-iteration-count", "1");
    $('#map').css("animation", "fadein 0.5s");
    $('#map').css("animation-iteration-count", "1");
    $('#map').css("-webkit-animation-fill-mode", "forwards");
    $('#map').css("animation-fill-mode", "forwards");
    $('#menupage').css("-webkit-animation", "fadeout 0.3s");
    $('#menupage').css("-webkit-animation-iteration-count", "1");
    $('#menupage').css("animation", "fadeout 0.3s");
    $('#menupage').css("animation-iteration-count", "1");
    $('#menupage').css("-webkit-animation-fill-mode", "forwards");
    $('#menupage').css("animation-fill-mode", "forwards");
    document.getElementById("map").style.display = "block";
    document.getElementById("menupage").style.display = "none";
    document.getElementById("menuitem").style.display = "none";
    document.getElementById("itemscont").innerHTML = "";
    map.setZoom((16), {animate: true});
};

function closeMenuNotSigned() {
    document.getElementById("lightboxmenu").style.display = "none";
    document.getElementById("signinformenu").style.display = "none";
};

function getMenuList() {
    shopList.child(shopIDGlobal).child("menuItems").child(currentCatInView).once("value", function(snapshot) {
        //gives the shop data a variable
        var menuItems = (snapshot.val());
        $.each(menuItems, function(menuItem, value){
            var itemName = value.itemName;
            var price = value.price;
                var menuItemDiv = document.createElement('div');
                menuItemDiv.setAttribute("id", menuItem);
                menuItemDiv.innerHTML = '<a href="javascript:void(0)" onclick="orderItem(this)"><div class="menuitemcont"></div><div class="divider"></div><div class="menuitemtext menuitemdrinks">' + itemName + '</div><div class="menuitemprice">£' + price + '</div></a>';
                document.getElementById('itemscont').appendChild(menuItemDiv);                
        })
    })
};

function orderItem(element, authData) {
//    console.log(authData);
//    if (authData) {
    if (window.userId == "") {
        document.getElementById("lightboxmenu").style.display = "block";
        document.getElementById("signinformenu").style.display = "block";
    } else {
        document.getElementById("lightboxmenu").style.display = "none";
        document.getElementById("signinformenu").style.display = "none";
    currentMenuItemId = element.parentNode.id;
    shopList.child(shopIDGlobal).child("menuItems").child(currentCatInView).child(currentMenuItemId).once("value", function(snapshot) {
        var itemInFirebase = (snapshot.val());
        var itemNameInFirebase = itemInFirebase.itemName;
        var currentPrice = itemInFirebase.price;
        document.getElementById("orderitem2").innerHTML = itemNameInFirebase + "<br>" + currentPrice + '<form><input type="hidden" name="menuItemId" value="' + currentMenuItemId +'"><input type="hidden" name="shopId" value="' + shopIDGlobal +'"><input type="hidden" name="itemCat" value="' + currentCatInView +'"><input type="hidden" name="menuItemName" value="' + itemNameInFirebase +'"><select id="qtydrop" name="quantity"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select><input type="button" id="qtysubmit" onclick="addToOrder(this.form)"></form>';
        document.getElementById("ordersubtitle").innerHTML = "From " + shopNameGlobal;
    });
    $('#lightbox').css("-webkit-animation", "fadeinlightbox 0.5s");
    $('#lightbox').css("-webkit-animation-iteration-count", "1");
    $('#lightbox').css("animation", "fadeinlightbox 0.5s");
    $('#lightbox').css("animation-iteration-count", "1");
    $('#lightbox').css("-webkit-animation-fill-mode", "forwards");
    $('#lightbox').css("animation-fill-mode", "forwards");
    document.getElementById("lightbox").style.display = "block";
    $('#orderingbox').css("-webkit-animation", "fadein 0.5s");
    $('#orderingbox').css("-webkit-animation-iteration-count", "1");
    $('#orderingbox').css("animation", "fadein 0.5s");
    $('#orderingbox').css("animation-iteration-count", "1");
    $('#orderingbox').css("-webkit-animation-fill-mode", "forwards");
    $('#orderingbox').css("animation-fill-mode", "forwards");
    document.getElementById("orderingbox").style.display = "block";
    fireBase.child("users").child(window.userId).child("orders").child("currentorder").child(shopIDGlobal).once("value", function(snapshot) {
            var currentOrder = (snapshot.val());
            var totalPrice = [];
            document.getElementById("rightcontitems").innerHTML = "";
            document.getElementById("leftcontitems").innerHTML = "";
            $.each(currentOrder, function(item, value){
                itemId = value.itemId;
                itemCat = value.itemCat;
                shopList.child(shopIDGlobal).child("menuItems").child(itemCat).child(itemId).once("value", function(snapshot) {
                    var shopPrice = (snapshot.val());
                    itemName = value.itemName;
                    quantity = value.quantity;
                    document.getElementById("orderempty").style.display = "none";
                    document.getElementById("currentorder").style.display = "block";
                    var itemOnOrder = document.createElement('div');
                    itemOnOrder.className = 'currentorderitem';
                    itemOnOrder.setAttribute("id", item);
                    itemOnOrder.innerHTML = itemName + " x " + quantity;
                    document.getElementById('leftcontitems').appendChild(itemOnOrder);
                    var price = shopPrice.price;
                    var priceInt = parseFloat(price);
                    var multiplyInt = priceInt *= quantity;
                    totalPrice.push(multiplyInt);
                    var qtyOnOrder = document.createElement('div');
                    qtyOnOrder.className = 'currentorderitem2';
                    qtyOnOrder.setAttribute("id", price);
                    qtyOnOrder.innerHTML = "£" + price;
                    document.getElementById('rightcontitems').appendChild(qtyOnOrder);
                    var total = 0;
                    $.each(totalPrice, function(){
                        total += this;
                    });
                    document.getElementById('total').innerHTML = "Total: £" + total.toFixed(2);
                })
            })
        })
//    } else {
//    console.log("user not logged in")   
//    }
    }
};

function addToOrderClicked() {
    document.getElementById("qtysubmit").click();
};

function clickLoginBut() {
    document.getElementById("formloginbut").click();
};

function clickLoginButMenu() {
    document.getElementById("formloginbutmenu").click();
};

function clickRegisterBut() {
    document.getElementById("formregisterbut").click();
};

function clickRegisterButMenu() {
    document.getElementById("formregisterbutmenu").click();
    document.getElementById("lightboxmenu").style.display = "none";
    document.getElementById("signinformenu").style.display = "none";
};

function clickNextBut() {
    document.getElementById("formnextbut").click();
};

function clickRegisterPageBut() {
    document.getElementById("formregisterpagebut").click();
};

function addToOrder(form){
        itemId = form.menuItemId.value;
        quantity = form.qtydrop.value;
        itemName = form.menuItemName.value;
        shopId = form.shopId.value;
        itemCat = form.itemCat.value;
        fireBase.child("users").child(window.userId).child("orders").child("currentorder").child(shopId).once("value", function(snapshot) {
        var currentOrder = (snapshot.val());
        console.log(currentOrder);
        fireBase.child("users").child(window.userId).child("orders").child("currentorder").child(shopId).child(itemId).set({
            itemId: itemId,
            itemName: itemName,
            itemCat: itemCat,
            quantity: quantity
        }, onComplete);
        })
};

var onComplete = function(error) {
    if (error) {
        console.log('Error Adding Item');
    } else {
        var totalPrice = [];
        fireBase.child("users").child(window.userId).child("orders").child("currentorder").child(shopIDGlobal).once("value", function(snapshot) {
            var currentOrder = (snapshot.val());
            document.getElementById("rightcontitems").innerHTML = "";
            document.getElementById("leftcontitems").innerHTML = "";
            $.each(currentOrder, function(item, value){
                itemId = value.itemId;
                itemCat = value.itemCat;
                shopList.child(shopIDGlobal).child("menuItems").child(itemCat).child(itemId).once("value", function(snapshot) {
                    var shopPrice = (snapshot.val());
                    itemName = value.itemName;
                    quantity = value.quantity;
                    document.getElementById("orderempty").style.display = "none";
                    document.getElementById("currentorder").style.display = "block";
                    var itemOnOrder = document.createElement('div');
                    itemOnOrder.className = 'currentorderitem';
                    itemOnOrder.setAttribute("id", item);
                    itemOnOrder.innerHTML = itemName + " x " + quantity;
                    document.getElementById('leftcontitems').appendChild(itemOnOrder);
                    var price = shopPrice.price;
                    var priceInt = parseFloat(price);
                    var multiplyInt = priceInt *= quantity;
                    totalPrice.push(multiplyInt);
                    var qtyOnOrder = document.createElement('div');
                    qtyOnOrder.className = 'currentorderitem2';
                    qtyOnOrder.setAttribute("id", price);
                    qtyOnOrder.innerHTML = "£" + price;
                    document.getElementById('rightcontitems').appendChild(qtyOnOrder);
                    var total = 0;
                    $.each(totalPrice, function(){
                        total += this;
                    });
                    document.getElementById('total').innerHTML = "Total: £" + total.toFixed(2);
                })
            })
        })
    }
};

//send the order to the retailer and put in their firebase
function sendOrder() {
    //take a snapshot of users current order for that shop
    fireBase.child("users").child(window.userId).child("orders").child("currentorder").child(shopIDGlobal).once("value", function(snapshot) {
        //give the whole order a variable
        console.log(orderToGo);
        var orderToGo = (snapshot.val());
        if (orderToGo == undefined) {
            window.alert("No items on order. Please add items to your order before you try to place it.");
        } else {
        //push the order to the shops current order list
        fireBase.child("shops").child(shopIDGlobal).child("orders").child("currentorders").child(userId).push({
            order: orderToGo,
            status: "Placed"
        }, function (error) {
            //if there is an error setting to the shops fiirebase
            if (error) {
            console.log("Data could not be saved." + error);
          } else {
            //if the order has successfully been placed
            console.log("Data saved successfully.");
            //take a snapshot of the order in the retailers firebase
            fireBase.child("shops").child(shopIDGlobal).child("orders").child("currentorders").child(userId).once("value", function(snapshot) {
                //give that order a variable
                var submittedOrder = (snapshot.val());
                //find the unique key firebase has saved it under
                for (var key in submittedOrder) {
                    //snapshot the key it's saved under
                    fireBase.child("shops").child(shopIDGlobal).child("orders").child("currentorders").child(userId).child(key).once("value", function(snapshot) {
                        var order = (snapshot.val());
                        //give a variable to the order stored inside
                        var orderContent = order.order;
                        //write that order back into the users orders in the active order folder under same ID
                        fireBase.child("users").child(window.userId).child("orders").child("activeorders").child(shopIDGlobal).child(key).set({
                        order: orderContent,
                        status: "Placed"
                        })
                        //update orders page
//                        populateOrdersPage();
                        document.getElementById("total").innerHTML = "";
                        document.getElementById("orderempty").style.display = "block";
                        closeOrderingBox();
                        orderSent();
                    })
                }
                //clear the users current order with that shop
                fireBase.child("users").child(window.userId).child("orders").child("currentorder").child(shopIDGlobal).remove();
            })
          }
        })
        }
    })
};

function closeOrderingBox() {
    document.getElementById("lightbox").style.display = "none";
    document.getElementById("orderingbox").style.display = "none";
};

function orderSent() {
    document.getElementById("menupage").style.display = "none";
    document.getElementById("menuitem").style.display = "none";
    document.getElementById("popupbackground").style.display = "none";
    document.getElementById("popuplightbox").style.display = "none";
    document.getElementById("textinpopup").style.display = "none";
    $('#orderpage').css("-webkit-animation", "fadein 0.5s");
    $('#orderpage').css("-webkit-animation-iteration-count", "1");
    $('#orderpage').css("animation", "fadein 0.5s");
    $('#orderpage').css("animation-iteration-count", "1");
    $('#orderpage').css("-webkit-animation-fill-mode", "forwards");
    $('#orderpage').css("animation-fill-mode", "forwards");
    document.getElementById("orderpage").style.display = "block";
    categories.on("value", function(snapshot) {
        var categoriesList = (snapshot.val());
        $.each(categoriesList, function(categoryId) {
            var destroyCat = document.getElementById(categoryId);
            if (destroyCat) {
            destroyCat.remove();
            }
        }) 
    })
    document.getElementById("itemscont").innerHTML = "";
};

function categoryTransition() {
    document.getElementById("shopheader").style.display = "none";
    $('#menuitem').css("-webkit-animation", "fadein 0.5s");
    $('#menuitem').css("-webkit-animation-iteration-count", "1");
    $('#menuitem').css("animation", "fadein 0.5s");
    $('#menuitem').css("animation-iteration-count", "1");
    $('#menuitem').css("-webkit-animation-fill-mode", "forwards");
    $('#menuitem').css("animation-fill-mode", "forwards");
    document.getElementById("menuitem").style.display = "block";
    $('#shoptitletext').css("-webkit-animation", "fadein 0.5s");
    $('#shoptitletext').css("-webkit-animation-iteration-count", "1");
    $('#shoptitletext').css("animation", "fadein 0.5s");
    $('#shoptitletext').css("animation-iteration-count", "1");
    $('#shoptitletext').css("-webkit-animation-fill-mode", "forwards");
    $('#shoptitletext').css("animation-fill-mode", "forwards");
    $('#shopheader').css("-webkit-animation", "fadein 1s");
    $('#shopheader').css("-webkit-animation-iteration-count", "1");
    $('#shopheader').css("animation", "fadein 1s");
    $('#shopheader').css("animation-iteration-count", "1");
    $('#shopheader').css("-webkit-animation-fill-mode", "forwards");
    $('#shopheader').css("animation-fill-mode", "forwards");
    document.getElementById("shopheader").style.display = "block";
    document.getElementById("categoriescont").style.display = "none";
    document.getElementById("loadinghold").style.display = "none";
};

//categories clicked
function bakerycatClicked() {
    currentCatInView = "bakery";
    document.getElementById("loadinghold").style.display = "block";
    var img = new Image();
    img.onload = function() { 
        document.getElementById("shoptitletext").innerHTML = "Bakery";
        document.getElementById("shopheader").src = "img/bakeryback.png";
        categoryTransition();
        }
    img.src = "img/bakeryback.png";
};

function hotdrinkscatClicked() {
    currentCatInView = "hotdrinks";
    document.getElementById("loadinghold").style.display = "block";
    var img = new Image();
    img.onload = function() { 
        document.getElementById("shoptitletext").innerHTML = "Hot Drinks";
        document.getElementById("shopheader").src = "img/hotdrinksback.png";
        categoryTransition();
        }
    img.src = "img/hotdrinksback.png";
};

function colddrinkscatClicked() {
    currentCatInView = "colddrinks";
    document.getElementById("loadinghold").style.display = "block";
    var img = new Image();
    img.onload = function() { 
        document.getElementById("shoptitletext").innerHTML = "Cold Drinks";
        document.getElementById("shopheader").src = "img/colddrinksback.png";
        categoryTransition();
        }
    img.src = "img/colddrinksback.png";
};

function fruitcatClicked() {
    currentCatInView = "fruit";
    document.getElementById("loadinghold").style.display = "block";
    var img = new Image();
    img.onload = function() { 
        document.getElementById("shoptitletext").innerHTML = "Fruit";
        document.getElementById("shopheader").src = "img/fruitback.png";
        categoryTransition();
        }
    img.src = "img/fruitback.png";
};

function lunchcatClicked() {
    currentCatInView = "lunch";
    document.getElementById("loadinghold").style.display = "block";
    var img = new Image();
    img.onload = function() { 
        document.getElementById("shoptitletext").innerHTML = "Lunch";
        document.getElementById("shopheader").src = "img/lunchback.png";
        categoryTransition();
        }
    img.src = "img/lunchback.png";
};

function snackscatClicked() {
    currentCatInView = "snacks";
    document.getElementById("loadinghold").style.display = "block";
    var img = new Image();
    img.onload = function() { 
        document.getElementById("shoptitletext").innerHTML = "Snacks";
        document.getElementById("shopheader").src = "img/snacksback.png";
        categoryTransition();
        }
    img.src = "img/snacksback.png";
};