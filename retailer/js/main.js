//creates firebase reference
var fireBase = new Firebase("https://proper-coffee.firebaseio.com/");

// Creating Reference to shoplist created by propercoffeeadmin
var shopList = new Firebase("https://proper-coffee.firebaseio.com/shops");

//creating reference to category list
var categories = new Firebase("https://proper-coffee.firebaseio.com/cats");

//Creating variable for loading spinner
var spinner = new Spinner({color: '#ddd'});

var userIDGlobal = [];
var shopImageGlobal = [];
var shopNameGlobal = [];
var currentCatInView = [];
var currentMenuItemId = [];

function logOut() {
    fireBase.unauth();
    document.getElementById("loginpage").style.display = "block";
    document.getElementById("infopage").style.display = "none";
    document.getElementById("orderpage").style.display = "none";
    document.getElementById("menubar").style.display = "none";
    window.userId = [];
};

//if user is logged in run auth data function
fireBase.onAuth(authDataCallback);

//detect current log in status
function authDataCallback(authData) {
  if (authData) {
    //if user is logged in show form, hide login page and log it
    console.log("User " + authData.uid + " is logged in with " + authData.provider);
        //create variable for the users unique user ID
        var userID = authData.uid;
        userIDGlobal = userID;
        window.userId = userID;
        //get the data from the entry in the shop list matching the user ID
        shopList.child(userID).once("value", function(snapshot) {
            //gives the shop data a variable
            var shopData = (snapshot.val());
            //gets shop name
            var shopName = shopData.shopName;
            shopNameGlobal = shopName;
            //changes title for shop
            document.getElementById("shoptitletext").innerHTML = shopName;
            //changes shop image
            var shopImage = shopData.shopImage;
            if (shopImage == null) {
            shopImageGlobal = "img/shopback.png";
            document.getElementById("shopimage").src = "img/shopback.png";
            } else {
            shopImageGlobal = shopImage;
            document.getElementById("shopimage").src = shopImage;
            }
            //gets menu category info
            var shopMenu = shopData.menu;
            $.each(shopMenu, function(item, value){
                //snap shot the same category from the cats databse
                categories.child(item).on("value", function(snapshot) {
                    var categoriesList = (snapshot.val());
                    //get the plain text category name
                    var categoryName = categoriesList.name;
                    var categoryDiv = document.createElement('div');
                    categoryDiv.className = 'categorycontainer';
                    categoryDiv.setAttribute("id", item);
                    categoryDiv.setAttribute("onclick", item + "catClicked();getMenuList();");
                    categoryDiv.innerHTML = '<a href="javascript:void(0)"><img id="' + item + 'add" class="addremovecategory" src="img/add.png"><img id="' + item +'remove" class="addremovecategory" src="img/remove.png"><img class="menucategory" src="img/' + item + '.png"><div class="menucatoverlay"></div><div class="menucattext">' + categoryName + '</div></a>';
                    document.getElementById('categoriescont').appendChild(categoryDiv);
                    if (value == 1) {
                        document.getElementById(item).style.display = "block";
                    } else {
                        document.getElementById(item).style.display = "none";
                    }
                })
            })     
        })
    document.getElementById("loginpage").style.display = "none";
    document.getElementById("orderpage").style.display = "block";
    document.getElementById("menubar").style.display = "block";
    } else {
    //if user is logged out log it, hide form and tell user to log in
    console.log("User logged out");
    }
};

//stop listening for authentication state changes
fireBase.offAuth(authDataCallback);

//switch page to orders page and close other pages.
function openOrdersPage() {
    document.getElementById("orderpage").style.display = "block";
    document.getElementById("infopage").style.display = "none";
};

//switch page to info page and close other pages.
function openInfoPage() {
    document.getElementById("orderpage").style.display = "none";
    document.getElementById("infopage").style.display = "block";
};

function printButPressed() {
    window.alert("No printer is connected. Add a printer in settings");
};

function editButPressed() {
    window.alert("This order is unavailable for editing");
};

//getting current orders to displays on orders page - snapshot of current orders in firebase
    shopList.child(window.userId).child("orders").child("currentorders").on("value", function(snapshot) {
        //set variable for whole list of current orders
        var currentOrders = (snapshot.val());
        if (currentOrders == null) {
            document.getElementById("noorders").style.display = "block";   
        } else {
            document.getElementById("noorders").style.display = "none";   
        }
        document.getElementById('orders').innerHTML = '';
        //for each current order entry get consumer id of who placed order
        $.each(currentOrders, function(consumerId){
            //get the data about that user from their entry in firebase
            fireBase.child("users").child(consumerId).once("value", function(snapshot) {
                //set a variable for consumber data
                var consumerData = (snapshot.val()); 
                var consumerFirstName = consumerData.firstname;
                //snapshot all current orders by that particular user
                shopList.child(window.userId).child("orders").child("currentorders").child(consumerId).on("value", function(snapshot) {
                    //variable for all of the current consumers orders
                    var consumersCurrentOrders =  (snapshot.val()); 
                    //get the order Id and what is on the order for each order that users placed
                    $.each(consumersCurrentOrders, function(orderId, orderValue){
                        //variable for order status
                        var orderStatus = orderValue.status;
                        //creates order div variable to be pushed later
                        var orderDiv = document.createElement('div');
                        //sets id of order div to the orders ID
                        orderDiv.setAttribute("id", orderId);
                        //creates the html for the order div
                        orderDiv.innerHTML = '<div class="activeorder"></div><div class="activeordername">' + consumerFirstName + '</div><div class="activeorderstatus">Status - Order ' + orderStatus + '</div><a href="javascript:void(0)" onclick="openOrderDetails(this)"><img src="img/downbut.png" id="' + orderId + 'downbut" class="downbut"></a><a href="javascript:void(0)" onclick="closeOrderDetails(this)"><img src="img/upbut.png" id="' + orderId + 'upbut" class="upbut"></a><div id="' + orderId + 'orderdetails" class="orderdetails"><div id="' + orderId + 'orderdetailsordertext" class="orderdetailsordertext">Items:<br></div></div><div id="' + orderId + 'orderbuttonscont" class="orderbuttonscont"><form name="orderDetails"><input type="hidden" name="customerUserId" value="' + consumerId +'"><input type="hidden" name="orderId" value="' + orderId +'"><input type="button" class="userIdSubmit" id="userIdSubmit' + orderId + '" onclick="acceptOrder(this.form)"></form><form name="orderDetails2"><input type="hidden" name="customerUserId" value="' + consumerId +'"><input type="hidden" name="orderId" value="' + orderId +'"><input type="button" class="userIdSubmit" id="userIdSubmit2' + orderId + '" onclick="readyOrder(this.form)"></form><form name="orderDetails3"><input type="hidden" name="customerUserId" value="' + consumerId +'"><input type="hidden" name="orderId" value="' + orderId +'"><input type="button" class="userIdSubmit" id="userIdSubmit3' + orderId + '" onclick="doneOrder(this.form)"></form><a href="javascript:void(0)" onclick="clickUserIdForm(this)"><img src="img/acceptbut.png" id="accept' + orderId +'" class="orderbutton"></a><a href="javascript:void(0)" onclick="clickUserIdForm2(this)"><img src="img/readybut.png" id="ready' + orderId +'" class="orderbutton readybut"></a><a href="javascript:void(0)" onclick="clickUserIdForm3(this)"><img src="img/donebut.png" id="done' + orderId +'" class="orderbutton readybut"></a><a href="javascript:void(0)" onclick="printButPressed()"><img src="img/printbut.png" class="orderbutton"></a><a href="javascript:void(0)" onclick="editButPressed()"><img src="img/editorderbut.png" class="orderbutton"></a></div><div class="divider"></div>';
                        //writes the order div to the HTML
                        document.getElementById('orders').appendChild(orderDiv);
                        //creates a variable for the contents of the order
                        var orderContents = orderValue.order;
                        //creates the correct buttons for status of order
                        if (orderStatus == "Placed") {
                            console.log("Placed");
                        } else if (orderStatus == "Accepted") {
                            document.getElementById("ready" + orderId).style.display = "inline";
                            document.getElementById("accept" + orderId).style.display = "none";
                            console.log("Accepted")
                        } else if (orderStatus == "Ready") {
                            document.getElementById("done" + orderId).style.display = "inline";
                            document.getElementById("accept" + orderId).style.display = "none";
                            console.log("Ready")
                        } else if (orderStatus == "Done") {
                            console.log("Done");
                        } else {
                            console.log("Status Error");    
                        };
                        //breaks the order down for each item in the order
                        $.each(orderContents, function(itemOnOrderId, itemOnOrder){
                            //creates variables for aspects of the order
                            var itemName = itemOnOrder.itemName;
                            var itemQuantity = itemOnOrder.quantity;
                            //creats a div for the order items
                            var itemOnOrderDiv = document.createElement('div');
                            //creates the html with the order item details in
                            itemOnOrderDiv.innerHTML = itemQuantity + " x " + itemName + '<br>';
                            //writes the order items to the HTML
                            document.getElementById(orderId + 'orderdetailsordertext').appendChild(itemOnOrderDiv);
                        })
                    })
                })
            })
        })
});

//expands order when order details button pressed
function openOrderDetails(element) {
    //gets orderId
    var orderId = element.parentNode.id;
    //makes changes
    document.getElementById(orderId + "upbut").style.display = "block";
    document.getElementById(orderId + "downbut").style.display = "none";
    document.getElementById(orderId + "orderdetails").style.display = "block";
    document.getElementById(orderId + "orderbuttonscont").style.display = "block";
};

//collapses order details when up button pressed
function closeOrderDetails(element) {
    //gets order id
    var orderId = element.parentNode.id;
    //makes changes
    document.getElementById(orderId + "downbut").style.display = "block";
    document.getElementById(orderId + "upbut").style.display = "none";
    document.getElementById(orderId + "orderdetails").style.display = "none";
    document.getElementById(orderId + "orderbuttonscont").style.display = "none";
};

//clicks the submit on the invisible for accept order
function clickUserIdForm(element) {
    var orderId = element.parentNode.parentNode.id;
    document.getElementById("userIdSubmit"  + orderId).click();
};

//clicks the submit on the invisible for ready order
function clickUserIdForm2(element) {
    var orderId = element.parentNode.parentNode.id;
    document.getElementById("userIdSubmit2"  + orderId).click();
};

//clicks the submit on the invisible for done order
function clickUserIdForm3(element) {
    var orderId = element.parentNode.parentNode.id;
    document.getElementById("userIdSubmit3"  + orderId).click();
};

//accepting order
function acceptOrder(form) {
    //gets order id
    var consumerId = form.customerUserId.value;
    var orderId = form.orderId.value;
    //get the order
    shopList.child(window.userId).child("orders").child("currentorders").child(consumerId).child(orderId).once("value", function(snapshot) {
        var orderToAccept = (snapshot.val());
        shopList.child(window.userId).child("orders").child("currentorders").child(consumerId).child(orderId).update({ status: 'Accepted'});
        fireBase.child("users").child(consumerId).child("orders").child("activeorders").child(window.userId).child(orderId).update({ status: 'Accepted'});
        //variable for all of the current consumers orders
    })
};

//ready order
function readyOrder(form) {
    //gets order id
    var consumerId = form.customerUserId.value;
    var orderId = form.orderId.value;
    //get the order
    shopList.child(window.userId).child("orders").child("currentorders").child(consumerId).child(orderId).once("value", function(snapshot) {
        var orderToAccept = (snapshot.val());
        shopList.child(window.userId).child("orders").child("currentorders").child(consumerId).child(orderId).update({ status: 'Ready'});
        fireBase.child("users").child(consumerId).child("orders").child("activeorders").child(window.userId).child(orderId).update({ status: 'Ready'});
        //variable for all of the current consumers orders
    })
};

//accepting order
function doneOrder(form) {
    //gets order id
    var consumerId = form.customerUserId.value;
    var orderId = form.orderId.value;
    //get the order
    shopList.child(window.userId).child("orders").child("currentorders").child(consumerId).child(orderId).once("value", function(snapshot) {
        var orderToAccept = (snapshot.val());
        shopList.child(window.userId).child("orders").child("oldorders").child(consumerId).child(orderId).set({
            order: orderToAccept.order,
            status: "Done"
        });
        fireBase.child("users").child(consumerId).child("orders").child("oldorders").child(window.userId).child(orderId).set({
            order: orderToAccept.order,
            status: "Done"
        });
        //variable for all of the current consumers orders
    })
    shopList.child(window.userId).child("orders").child("currentorders").child(consumerId).child(orderId).remove();
    fireBase.child("users").child(consumerId).child("orders").child("activeorders").child(window.userId).child(orderId).remove();
};


//switch page to info page and close other pages.
function openRegisterPage() {
    document.getElementById("loginpage").style.display = "none";
    document.getElementById("registerpage").style.display = "block";
};

//opens menu editing page 1 with categories on
function openMenuEdit() {
    document.getElementById("infopage1").style.display = "none";
    document.getElementById("menuedit1").style.display = "block";
    document.getElementById("editbut").style.display = "none";
};

function menuBack() {
    document.getElementById("infopage1").style.display = "block";
    document.getElementById("menuedit1").style.display = "none";
    document.getElementById("editbut").style.display = "block" 
};

function menuBack2() {
    document.getElementById("menuedit1").style.display = "block";
    document.getElementById("menuedit3").style.display = "none";
    document.getElementById("shoptitletext").innerHTML = shopNameGlobal;
    document.getElementById("shopimage").src = shopImageGlobal;
    document.getElementById("itemscont").innerHTML = "";
};

//edit category button pressed so switch to category edit mode
function addCategory() {
    document.getElementById("editcategory").setAttribute("onclick", "closeEdit()");
    shopList.child(userIDGlobal).on("value", function(snapshot) {
        //gives the shop data a variable
        var shopData = (snapshot.val());
        var shopMenu = shopData.menu;
        //snap shot the same category from the cats databse
        categories.on("value", function(snapshot) {
                //full list of possible categories
                var categoriesList = (snapshot.val());
                $.each(categoriesList, function(categoryId){
                    shopList.child(userIDGlobal).child("menu").child(categoryId).on("value", function(snapshot) {
                        var catValue = (snapshot.val());
                    if (catValue == 1) {
                        document.getElementById(categoryId + "remove").style.display = "block";
                        document.getElementById(categoryId).setAttribute("onclick", categoryId + "removeCat()");
                    } else {
                        document.getElementById(categoryId).style.display = "block";
                        document.getElementById(categoryId + "add").style.display = "block";
                        document.getElementById(categoryId).setAttribute("onclick", categoryId + "addCat()");
                    }
                })
            })
        })
    })
};

//edit category button pressed when done editing switch back to normal view
function closeEdit() {
    categories.on("value", function(snapshot) {
        var categoriesList = (snapshot.val());
        $.each(categoriesList, function(categoryId){
            shopList.child(userIDGlobal).child("menu").child(categoryId).on("value", function(snapshot) {
                var catValue = (snapshot.val());
                if (catValue == 1) {
                        document.getElementById(categoryId + "remove").style.display = "none";
                        document.getElementById(categoryId + "add").style.display = "none";
                        document.getElementById(categoryId).style.display = "block";
                        document.getElementById(categoryId).setAttribute("onclick", categoryId + "catClicked()");
                    } else {
                        document.getElementById(categoryId).style.display = "none";
                    }
                document.getElementById("editcategory").setAttribute("onclick", "addCategory()");
                })
            })
        })
    
};

//UNUSED LIGHTBOX
//function closeCategory() {
//    document.getElementById("lightbox").style.display = "none";
//    document.getElementById("catselectbox").style.display = "none";
//};

//When user submits log in form
function loginFormSubmitted(form){
    //get elements from log in form and give them variables
    var email = form.email.value;
    var password = form.password.value;
    //log in using firebase authentication
    fireBase.authWithPassword({
        email : email,
        password : password
    }, function(error, authData) {
        //if log in is unsuccessful
        if (error) {
            console.log("Login Failed!", error);
            document.getElementById("loginstatus").innerHTML = "Login Failed: Username or Password Incorrect";
        //if log in is successful
        } else {
            console.log("Authenticated successfully with payload:", authData);
            //create variable for the users unique user ID
            var userID = authData.uid;
            //get the data from the entry in the shop list matching the user ID
            shopList.child(userID).on("value", function(snapshot) {
                //gives the shop data a variable
                var shopData = (snapshot.val());
                //detects first if the user has logged in before
                var firstLogin = shopData.firstlogin;
                if (firstLogin == 1) {
                    document.getElementById("loginpage").style.display = "none";
                    document.getElementById("passwordchangepage").style.display = "block";
                } else {
                    location.reload();
                }
            });
        }
    })
};

//changing the users password
function changePassword(form) {
    //set variables for info needed
    var authData = fireBase.getAuth();
    var userID = authData.uid;
    var email = authData.password.email;
    var password = form.password.value;
    var newPassword = form.newpassword.value;
    var newVerifyPassword = form.newverifypassword.value;
    //if new password is valid change password
    if (newPassword == newVerifyPassword){
        fireBase.changePassword({
            email : email,
            oldPassword : password,
            newPassword : newPassword
            //if no errors changing password
        }, function(error) {
            if (error === null) {
                shopList.child(userID).update({
                    firstlogin: 0
                });
                location.reload();
                //if error with changing passwords
            } else {
                document.getElementById("passwordchangestatus").innerHTML = "Error Changing Passwords<br> Please check try again.<br>";
                //resets form to try again
                document.changepasswordform.reset();
            }
            });
    } else {
        document.getElementById("passwordchangestatus").innerHTML = "New Passwords don't match.<br> Please try again.<br>";
    }
}

//run upload image click when edit button is clicked
function chooseFile() {
      document.getElementById("file-upload").click();
};

//handle file selected for upload to shopimage
function handleFileSelect(evt) {
    //set variable for selected image 
    var f = evt.target.files[0];
    //create a file reader
    var reader = new FileReader();
    //once reader is loaded run loading spinner function
    reader.onload = (function(theFile) {
        return function(e) {
            var filePayload = e.target.result;
            var authData = fireBase.getAuth();
            var userID = authData.uid;
            //create new firebase for path image needs to be saved in
            var f = new Firebase(shopList + '/' + userID + '/shopImage');
            spinner.spin(document.getElementById('spin'));
            //set image to firebase as base64 string
            f.set(filePayload, function() {
                //remove spinning loader
                spinner.stop();
                //set shop image to image in firebase
                document.getElementById("shopimage").src = e.target.result;
                shopImageGlobal = e.target.result;
            });
        };
    })(f);
  reader.readAsDataURL(f);
}

//loading spinner whilst file uploads
$(function() {
  $('#spin').append(spinner);
    document.getElementById("file-upload").addEventListener('change', handleFileSelect, false);
    spinner.spin(document.getElementById('spin'));
    spinner.stop();
});

function plusClicked() {
    document.getElementById("addbutton").click();
};

//adds item from form to menu list
function addItem(form) {
    item = form.item.value;
    price = form.price.value;
    shopList.child(userIDGlobal).child("menuItems").child(currentCatInView).push({
        itemName: item,
        price: price
    });
    document.getElementById("newitemform").reset();
    getMenuList();
}

function getMenuList() {
    shopList.child(userIDGlobal).child("menuItems").child(currentCatInView).once("value", function(snapshot) {
        //gives the shop data a variable
        var menuItems = (snapshot.val());
        document.getElementById('itemscont').innerHTML = "";
        $.each(menuItems, function(menuItem, value){
            var itemName = value.itemName;
            var price = value.price;
            var menuItemDiv = document.createElement('div');
            menuItemDiv.setAttribute("id", menuItem);
            menuItemDiv.innerHTML = '<div class="menuitemcont menuitemorange"></div><div class="divider"></div><div id="' + menuItem + 'Static"><div class="menuitemtext menuitemdrinks">' + itemName + '</div><div class="menuitemprice">£' + price + '</div></div><a href="javascript:void(0)" onclick="editMenuItem(this)"><img class="editmenuitem" src="img/editicon.png"></a><div class="edititemform" id="' + menuItem + 'Edit"></div>';
                document.getElementById('itemscont').appendChild(menuItemDiv);                
        })
    })
};

function editMenuItem(element) {
    var currentMenuItemId = element.parentNode.id;
    document.getElementById(currentMenuItemId + "Static").style.display = "none";
    shopList.child(userIDGlobal).child("menuItems").child(currentCatInView).child(currentMenuItemId).once("value", function(snapshot) {
        var menuItemInfo = (snapshot.val());
        var itemName = menuItemInfo.itemName;
        var price = menuItemInfo.price;
        document.getElementById(currentMenuItemId + "Edit").innerHTML = '<form id="edititemform"><input class="editmenuitemname" name="item" value="' + itemName + '" type="text"><input class="editmenuitemprice" name="price" value="' + price + '" type="text"><input type="button" id="editbutton" value="Submit" onclick="changeItem(this.form)"></form><a href="javascript:void(0)" onclick="doneEditingItem(this)"><img class="editmenuitemdone" src="img/editicon.png"></a><a href="javascript:void(0)" onclick="removeItem(this)"><img class="removemenuitem" src="img/removesmall.png"></a>';
        document.getElementById(currentMenuItemId + "Edit").style.display = "block";
    })
};

function removeItem(element) {
    var editMenuItemParentDiv = element.parentNode;
    currentMenuItemId = editMenuItemParentDiv.parentNode.id;
    shopList.child(userIDGlobal).child("menuItems").child(currentCatInView).child(currentMenuItemId).remove();
    document.getElementById(currentMenuItemId + "Static").style.display = "block";
    document.getElementById(currentMenuItemId + "Edit").style.display = "none";
    getMenuList();
}

function doneEditingItem(element) {
    var editMenuItemParentDiv = element.parentNode;
    currentMenuItemId = editMenuItemParentDiv.parentNode.id;
    document.getElementById("editbutton").click();
    document.getElementById(currentMenuItemId + "Static").style.display = "block";
    document.getElementById(currentMenuItemId + "Edit").style.display = "none";
};

function changeItem(form) {
    item = form.item.value;
    price = form.price.value;
    shopList.child(userIDGlobal).child("menuItems").child(currentCatInView).push({
        itemName: item,
        price: price
    }, onComplete);
    document.getElementById("newitemform").reset();
}

var onComplete = function(error) {
    if (error) {
        console.log('Error Editing Item');
    } else {
        shopList.child(userIDGlobal).child("menuItems").child(currentCatInView).child(currentMenuItemId).remove();
        getMenuList();
        }
};

function bakerycatClicked() {
    currentCatInView = "bakery";
    document.getElementById("shoptitletext").innerHTML = "Bakery";
    document.getElementById("shopimage").src = "img/bakeryback.png";
    document.getElementById("menuedit3").style.display = "block";
    document.getElementById("menuedit1").style.display = "none";
};

function hotdrinkscatClicked() {
    currentCatInView = "hotdrinks";
    document.getElementById("shoptitletext").innerHTML = "Hot Drinks";
    document.getElementById("shopimage").src = "img/hotdrinksback.png";
    document.getElementById("menuedit3").style.display = "block";
    document.getElementById("menuedit1").style.display = "none";
};

function colddrinkscatClicked() {
    currentCatInView = "colddrinks";
    document.getElementById("shoptitletext").innerHTML = "Cold Drinks";
    document.getElementById("shopimage").src = "img/colddrinksback.png";
    document.getElementById("menuedit3").style.display = "block";
    document.getElementById("menuedit1").style.display = "none";
};

function fruitcatClicked() {
    currentCatInView = "fruit";
    document.getElementById("shoptitletext").innerHTML = "Fruit";
    document.getElementById("shopimage").src = "img/fruitback.png";
    document.getElementById("menuedit3").style.display = "block";
    document.getElementById("menuedit1").style.display = "none";
};

function lunchcatClicked() {
    currentCatInView = "lunch";
    document.getElementById("shoptitletext").innerHTML = "Lunch";
    document.getElementById("shopimage").src = "img/lunchback.png";
    document.getElementById("menuedit3").style.display = "block";
    document.getElementById("menuedit1").style.display = "none";
};

function snackscatClicked() {
    currentCatInView = "snacks";
    document.getElementById("shoptitletext").innerHTML = "Snacks";
    document.getElementById("shopimage").src = "img/snacksback.png";
    document.getElementById("menuedit3").style.display = "block";
    document.getElementById("menuedit1").style.display = "none";
};

//Category add remove clicks
function bakeryremoveCat() {
    shopList.child(userIDGlobal).child("menu").update({
        bakery: 0
    })
    document.getElementById("bakeryremove").style.display = "none";
    document.getElementById("bakeryadd").style.display = "block";
};

function bakeryaddCat() {
    shopList.child(userIDGlobal).child("menu").update({
        bakery: 1
    })
    document.getElementById("bakeryadd").style.display = "none";
    document.getElementById("bakeryremove").style.display = "block";
};

function hotdrinksremoveCat() {
    shopList.child(userIDGlobal).child("menu").update({
        hotdrinks: 0
    })
    document.getElementById("hotdrinksremove").style.display = "none";
    document.getElementById("hotdrinksadd").style.display = "block";
};

function hotdrinksaddCat() {
    shopList.child(userIDGlobal).child("menu").update({
        hotdrinks: 1
    })
    document.getElementById("hotdrinksadd").style.display = "none";
    document.getElementById("hotdrinksremove").style.display = "block";
};

function colddrinksremoveCat() {
    shopList.child(userIDGlobal).child("menu").update({
        colddrinks: 0
    })
    document.getElementById("colddrinksremove").style.display = "none";
    document.getElementById("colddrinksadd").style.display = "block";
};

function colddrinksaddCat() {
    shopList.child(userIDGlobal).child("menu").update({
        colddrinks: 1
    })
    document.getElementById("colddrinksadd").style.display = "none";
    document.getElementById("colddrinksremove").style.display = "block";
};

function fruitremoveCat() {
    shopList.child(userIDGlobal).child("menu").update({
        fruit: 0
    })
    document.getElementById("fruitremove").style.display = "none";
    document.getElementById("fruitadd").style.display = "block";
};

function fruitaddCat() {
    shopList.child(userIDGlobal).child("menu").update({
        fruit: 1
    })
    document.getElementById("fruitadd").style.display = "none";
    document.getElementById("fruitremove").style.display = "block";
};

function snacksremoveCat() {
    shopList.child(userIDGlobal).child("menu").update({
        snacks: 0
    })
    document.getElementById("snacksremove").style.display = "none";
    document.getElementById("snacksadd").style.display = "block";
};

function snacksaddCat() {
    shopList.child(userIDGlobal).child("menu").update({
        snacks: 1
    })
    document.getElementById("snacksadd").style.display = "none";
    document.getElementById("snacksremove").style.display = "block";
};

function lunchremoveCat() {
    shopList.child(userIDGlobal).child("menu").update({
        lunch: 0
    })
    document.getElementById("lunchremove").style.display = "none";
    document.getElementById("lunchadd").style.display = "block";
};

function lunchaddCat() {
    shopList.child(userIDGlobal).child("menu").update({
        lunch: 1
    })
    document.getElementById("lunchadd").style.display = "none";
    document.getElementById("lunchremove").style.display = "block";
};